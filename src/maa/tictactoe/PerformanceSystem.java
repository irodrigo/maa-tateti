package maa.tictactoe;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The Performance System is the module that must solve the given performance task, in this case playing tic-tac-toe,
 * by using the learned target function(s). It takes an instance of a new problem (new game) as input and produces a
 * trace of its solution (game history) as output. In our case, the strategy used by the Performance System to select
 * it's next move at each step is determined by the learned evaluation function. Therefore, we expect its performance
 * to improve as this evaluation function becomes increasingly accurate.
 *
 * Created by irodrigo on 8/23/15.
 */
public class PerformanceSystem {

    /**
     * Play a game with the given starting board, initial weights and starting player.
     * @param board The initial board.
     * @param weights The initial weights.
     * @param startingPlayer The mark of the starting player.
     * @param randomize Flag to indicate if players pick the best move randomly from all the best moves.
     * @return A list of all board states from the initial state to the final board.
     */
    public List<Board> play(Board board, double[] weights, Mark startingPlayer, boolean randomize) {

        // create players
        LearningPlayer player1 = new LearningPlayer(startingPlayer, weights.clone(), randomize);
        LearningPlayer player2 = new LearningPlayer(startingPlayer.opposite(), weights.clone(), randomize);


        // play game
        List<Board> history = new ArrayList<Board>();
        history.add(board);
        Board currentBoard = board;

        while(!currentBoard.isFinished()){

            Board board1 = player1.play(currentBoard);
            history.add(board1);
            currentBoard = board1;

            if (!currentBoard.isFinished()) {
                Board board2 = player2.play(currentBoard);
                history.add(board2);
                currentBoard = board2;
            }
        }

        return history;
    }

}
