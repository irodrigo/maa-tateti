package maa.tictactoe;

import java.util.*;

/**
 * This class represents a board of any given size.
 * It provides methods for setting and getting the mark in a given coordinate, and also to retrieve various relevant
 * board state properties like the the empty coordinates, all the lines, the lines with winning chance for a given mark,
 * the features that represent the board, the finished state and the winner.
 *
 * Created by irodrigo on 8/20/15.
 */
class Board {

    public static int FEATURE_COUNT = 7;

    private int size; // size of the board (square)
    private Mark[][] table; // the actual table, null is empty

    public Board(int size) throws IllegalArgumentException {
        if (size < 3) {
            throw new IllegalArgumentException("Board size must be greater than or equal to 3");
        }
        this.size = size;
        this.table = new Mark[size][size]; // init as null for empty table
    }

    public Board(Board board) throws IllegalArgumentException {
        if (board == null) {
            throw new IllegalArgumentException("Can't copy a null board");
        }
        this.size = board.size;
        this.table = new Mark[size][size];
        for (int row = 0; row < size; row++){
            for (int col = 0; col < size; col++){
                this.table[row][col] = board.table[row][col];
            }
        }
    }

    public int getSize(){
        return size;
    }

    public Mark getMark(int row, int column){
        return table[row][column];
    }

    /**
     * Return all the coordinates where no mark has been set yet.
     * @return The empty coordinates.
     */
    public Set<Coordinate> emptyCoordinates(){
        Set<Coordinate> emptyCoordinates = new HashSet<Coordinate>();
        for(int row = 0; row < size; row++){
            for(int col = 0; col < size; col++){
                if (table[row][col] == null) {
                    emptyCoordinates.add(new Coordinate(row, col));
                }
            }
        }
        return emptyCoordinates;
    }

    /**
     * Set the given mark at the given row and column.
     * @param row The row to set the mark.
     * @param column The column to set the mark.
     * @param mark The mark to set.
     * @throws IllegalArgumentException If the coordinate already has a mark or if no mark is provided.
     * @throws ArrayIndexOutOfBoundsException If the given coordinate is outside the board boundaries.
     */
    public void setMark(int row, int column, Mark mark) throws IllegalArgumentException, ArrayIndexOutOfBoundsException {
        if (table[row][column] != null) {
            throw new IllegalArgumentException("Coordinate already set");
        }
        if (mark == null) {
            throw new IllegalArgumentException("mark can't be null");
        }
        table[row][column] = mark;
    }

    /**
     * Return all the lines in the board.
     * This is a set containing all rows, all columns and the main diagonals.
     * @return All the lines in the board.
     */
    public Set<List<Coordinate>> allLines(){

        Set<List<Coordinate>> lines = new HashSet<List<Coordinate>>();

        // horizontal lines
        for (int r = 0; r < size; r++){
            List<Coordinate> horizontalLine = new ArrayList<Coordinate>();
            for(int c = 0; c < size; c++){
                horizontalLine.add(new Coordinate(r, c));
            }
            lines.add(horizontalLine);
        }

        // vertical lines
        for (int c = 0; c < size; c++){
            List<Coordinate> verticalLine = new ArrayList<Coordinate>();
            for(int r = 0; r < size; r++){
                verticalLine.add(new Coordinate(r, c));
            }
            lines.add(verticalLine);
        }

        // diagonal lines
        List<Coordinate> diagonal = new ArrayList<Coordinate>();
        List<Coordinate> reverseDiagonal = new ArrayList<Coordinate>();
        for(int i = 0; i < size; i++){
            diagonal.add(new Coordinate(i, i));
            reverseDiagonal.add(new Coordinate(i, size - 1 - i));
        }
        lines.add(diagonal);
        lines.add(reverseDiagonal);

        return lines;
    }

    /**
     * If count > 0, calculates the lines that have a chance to win, with exactly `count` marks of the given type.
     * If count <= 0, calculates all lines with winning chance, with any number of marks.
     * A line has a chance to win if it has at least one mark of the given type, and no marks of the opposite type.
     * @param mark The mark.
     * @param count The number of marks of the given type in a line, or 0 if any number of marks.
     * @return A set of all lines with winning chance.
     * @throws IllegalArgumentException If count is greater than the board size or no mark is provided.
     */
    public Set<List<Coordinate>> calculateLinesWithWinningChance(Mark mark, int count) throws IllegalArgumentException {

        if (count > size) {
            throw new IllegalArgumentException("count must be less than or equal to the board size");
        }
        if (mark == null) {
            throw new IllegalArgumentException("mark can't be null");
        }

        Set<List<Coordinate>> lines = new HashSet<>();

        for (List<Coordinate> line : allLines()) {
            int c = 0;
            boolean canWin = true;
            for (Coordinate coord : line) {
                if (getMark(coord.row, coord.column) == mark) {
                    c++;
                }
                else if (getMark(coord.row, coord.column) == mark.opposite()) {
                    canWin = false;
                    break;
                }
            }
            if (canWin && c > 0 && (count <= 0 || count == c)) {
                lines.add(line);
            }
        }

        return lines;
    }

    /**
     * Get the winner mark if any. A mark is the winner if there's a line full of them.
     * @return The winner if any or null if the board is not finished or draw.
     */
    public Mark winner(){
        for (Mark m : Mark.values()){
            if(calculateLinesWithWinningChance(m, size).size() > 0){
                return m;
            }
        }
        return null;
    }

    /**
     * Check if the board is finished. A board is finished if there's a winner or if there are no empty marks (draw game).
     * @return True iff the board is finished.
     */
    public boolean isFinished() {
        return emptyCoordinates().isEmpty() || winner() != null;
    }

    /**
     * Get the features of the board for the given mark.
     * The features are a simplification of the state of the board as seen by the player with the given mark.
     * Every board has Board.FEATURE_COUNT features.
     * The features x0..xN are calculated following these rules
     * - x0 = 1 (for w0)
     * - x1 = number of lines with winning chance for this player
     * - x2 = number of lines with winning chance for the opponent
     * - x3 = number of lines with winning chance for this player, that are (this.boardSize - 1) long
     * - x4 = number of lines with winning chance for the opponent, that are (this.boardSize - 1) long
     * - x5 = number of winning lines for this player (game over)
     * - x6 = number of winning lines for the opponent (game over)
     * @param mark The mark.
     * @return An array with the board's features for the given mark.
     */
    public int[] features(Mark mark){
        int[] features = new int[Board.FEATURE_COUNT];
        features[0] = 1;
        int[] lineLengths = {0, -1, -1, size - 1, size - 1, size, size};
        for(int i = 1; i < lineLengths.length; i++) {
            Mark currentMark = (i % 2 == 0) ? mark.opposite() : mark;
            int lines = calculateLinesWithWinningChance(currentMark, lineLengths[i]).size();
            features[i] = lines;
        }
        return features;
    }

    @Override
    public String toString() {
        String str = "";
        for (int r = 0; r < size; r++) {
            for (int c = 0; c < size; c++) {
                Mark mark = getMark(r, c);
                if (mark == null) {
                    str += " _ ";
                }
                else {
                    str += " " + mark.toString() + " ";
                }
            }
            str += "\n";
        }
        return str;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Board board = (Board) o;

        if (size != board.size) {
            return false;
        }
        return Arrays.deepEquals(table, board.table);

    }

    @Override
    public int hashCode() {
        return 31 * size + Arrays.deepHashCode(table);
    }
}
