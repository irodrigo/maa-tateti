package maa.tictactoe;

/**
 * Basic data type to represent a coordinate in a board.
 *
 * Created by irodrigo on 8/19/15.
 */
public class Coordinate {

    public int row;
    public int column;

    public Coordinate(int row, int column){
        this.row = row;
        this.column = column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        Coordinate that = (Coordinate)o;
        return row == that.row && column == that.column;
    }

    @Override
    public int hashCode() {
        return 31 * row + column;
    }
}
