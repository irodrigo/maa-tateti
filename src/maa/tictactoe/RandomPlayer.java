package maa.tictactoe;

import java.util.Random;
import java.util.Set;

/**
 * A tic-tac-toe player that randomly picks the next move from all valid moves every turn.
 *
 * Created by irodrigo on 8/20/15.
 */
public class RandomPlayer extends Player {

    public RandomPlayer(Mark mark) {
        super(mark);
    }

    @Override
    public Coordinate nextMove(Board board) {
        if (board.isFinished()) {
            return null;
        }
        Set<Coordinate> emptyCoordinates = board.emptyCoordinates();
        Random randomizer = new Random();
        int random = randomizer.nextInt(emptyCoordinates.size());
        return (Coordinate) emptyCoordinates.toArray()[random];
    }

}
