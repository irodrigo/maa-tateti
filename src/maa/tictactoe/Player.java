package maa.tictactoe;

/**
 * Base abstract class for representing a tic-tac-toe player.
 * Created by irodrigo on 8/20/15.
 */
public abstract class Player {

    private Mark mark;

    public Player(Mark mark){
        assert(mark != null);
        this.mark = mark;
    }

    public Mark getMark(){
        return mark;
    }

    public abstract Coordinate nextMove(Board board);

    /**
     * Update the board by placing the player mark in the calculated coordinate for the next move.
     * The next move is calculated using the abstract function nextMove.
     * @param board The board.
     * @return The new board after making the next move.
     */
    public Board play(Board board){
        Coordinate coord = nextMove(board);
        Board boardCopy = new Board(board);
        if(coord != null){
            boardCopy.setMark(coord.row, coord.column, mark);
        }
        return boardCopy;
    }

    @Override
    public String toString() {
        return mark + " - " + getClass().getSimpleName();
    }
}
