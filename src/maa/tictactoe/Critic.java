package maa.tictactoe;

import java.util.*;

/**
 * The Critic takes as input the history or trace of the game and produces as output a set of training examples of the
 * target function.
 * Each training example in this case corresponds to some game state in the trace, along with an estimate Vtrain of the
 * target function value for this example.
 *
 * Created by irodrigo on 8/23/15.
 */
public class Critic {

    /**
     * Calculate the training examples from the given history, for a player s=with the given mark, using the given weights.
     * @param history The game history. First board must be empty and last board must be finished.
     * @param mark The mark of the player that is going to use the training examples.
     * @param weights The current weights.
     * @param randomize True if the player adds a random factor when picking the best move.
     * @return A list with the training examples.
     * @throws IllegalArgumentException If the history is empty, the mark is null or the number of weights doesn't match
     * Board.FEATURE_COUNT.
     */
    public List<TrainingExample> trainingExamples(List<Board> history, Mark mark, double[] weights, boolean randomize) throws IllegalArgumentException {

        if (history.isEmpty()) {
            throw new IllegalArgumentException("history is empty");
        }
        if (mark == null) {
            throw new IllegalArgumentException("mark is null");
        }
        if (weights.length != Board.FEATURE_COUNT) {
            throw new IllegalArgumentException("weights array should have Board.FEATURE_COUNT elements");
        }

        List<TrainingExample> examples = new ArrayList<TrainingExample>();
        LearningPlayer player = new LearningPlayer(mark, weights, randomize);

        int startIndex = 0;
        if (history.get(1).calculateLinesWithWinningChance(mark, 0).size() == 0) {
            // mark was not the starting player, need to start evaluating history at index 1
            startIndex = 1;
        }

        // calculate training values for intermediate boards
        double trainingValue;
        for (int i = startIndex; i < history.size(); i += 2) {
            Board board = history.get(i);
            if (i + 2 < history.size()) {
                // board is intermediate, calculate training value using next board
                Board nextBoard = history.get(i + 2);
                trainingValue = player.value(nextBoard);
                examples.add(new TrainingExample(board, trainingValue));
            }
        }

        // calculate training value for final board
        Board finalBoard = history.get(history.size() - 1);
        if (!finalBoard .isFinished()) {
            throw new IllegalArgumentException("last board is not finished");
        }

        Mark winner = finalBoard.winner();
        if (winner == mark){
            trainingValue = 100;
        }
        else if (winner == mark.opposite()){
            trainingValue = -100;
        }
        else {
            trainingValue = 0;
        }
        examples.add(new TrainingExample(finalBoard, trainingValue));

        return examples;
    }

}
