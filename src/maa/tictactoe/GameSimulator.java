package maa.tictactoe;

import java.util.List;
import java.util.Random;

/**
 * Basic module for simulating games and returning the results.
 *
 * Created by irodrigo on 8/20/15.
 */
public class GameSimulator {

    private int games;
    private int boardSize;

    public GameSimulator(int games, int boardSize) throws IllegalArgumentException{
        if (games <= 0) {
            throw new IllegalArgumentException("games must be positive");
        }
        if (boardSize < 3) {
            throw new IllegalArgumentException("board size must be at least 3");
        }

        this.games = games;
        this.boardSize = boardSize;
    }

    /**
     * Simulate games and learn in the process.
     * @param initialWeights The initial weights for the learning players.
     * @param learningRate The learning rate in [0, 1).
     * @param graduallyDecreaseLearningRate Flag to decrease the learning rate after every game.
     * @param randomize Learning players are created with this flag.
     * @return The results of the games.
     * @throws IllegalArgumentException If the learning rate is not in the [0, 1) range or initial weights count don't
     * match Board.FEATURE_COUNT.
     */
    public GameSimulatorResults learn(double[] initialWeights, double learningRate, boolean graduallyDecreaseLearningRate, boolean randomize) throws IllegalArgumentException {

        if (learningRate < 0 || learningRate > 1) {
            throw new IllegalArgumentException("learning rate must be between 0 and 1");
        }
        if (initialWeights.length != Board.FEATURE_COUNT) {
            throw new IllegalArgumentException("initial weights must be Board.FEATURE_COUNT long");
        }

        ExperimentGenerator experimentGenerator = new ExperimentGenerator(boardSize);
        PerformanceSystem performanceSystem = new PerformanceSystem();
        Critic critic = new Critic();
        Generalizer generalizer = new Generalizer();

        Random random = new Random();
        GameSimulatorResults results = new GameSimulatorResults();
        double[] currentWeights = initialWeights.clone();

        for (int g = 0; g < games; g++) {

            // generate initial board
            Board board = experimentGenerator.generateExperiment();

            // alternate starting player to avoid bias
            Mark[] marks = Mark.values();
            Mark startingPlayer = marks[random.nextInt(marks.length)];

            // play games
            List<Board> history = performanceSystem.play(board, initialWeights, startingPlayer, randomize);

            // update results
            Board finalBoard = history.get(history.size() - 1);
            Mark winner = finalBoard.winner();
            results.update(winner);

            // generate training data
            List<TrainingExample> trainingExamples = critic.trainingExamples(history, startingPlayer, currentWeights, randomize);

            // update weights
            currentWeights = generalizer.updatedWeights(trainingExamples, startingPlayer, currentWeights, learningRate, randomize);

            // decrease learning rate if needed
            if (graduallyDecreaseLearningRate) {
                learningRate -= learningRate / games;
            }
        }

        results.setFinalWeights(currentWeights);

        return results;
    }

    /**
     * Simulate games using the given players without learning anything.
     * @param players The two players.
     * @return The game results.
     * @throws IllegalArgumentException If player count is not 2 or players have the same marks.
     */
    public GameSimulatorResults simulate(Player[] players) throws IllegalArgumentException {

        if (players.length != 2) {
            throw new IllegalArgumentException("two players are needed for the simulation");
        }
        if (players[0].getMark() == players[1].getMark()) {
            throw new IllegalArgumentException("players must have different marks");
        }

        GameSimulatorResults results = new GameSimulatorResults();

        for (int g = 0; g < games; g++) {
            // generate empty board
            Board board = new Board(boardSize);

            // alternate starting player to avoid bias
            Player tmp = players[0];
            players[0] = players[1];
            players[1] = tmp;

            while (!board.isFinished()) {
                // play turns
                board = players[0].play(board);
                if (!board.isFinished()) {
                    board = players[1].play(board);
                }
            }

            // update results
            results.update(board.winner());
        }

        return results;
    }
}
