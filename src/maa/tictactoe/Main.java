package maa.tictactoe;


import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.util.Arrays;


public class Main {

    public static void main(String[] args) {

        // training and testing parameters
        int boardSize = 3;
        int trainingGames = 1000;
        double learningRate = 0.01;
        boolean graduallyDecreaseLearningRate = true;
        boolean randomize = true;
        int testGames = 1000;
        double[] initialWeights = {0, 0, 0, 0, 0, 0, 0};

        // train
        System.out.println();
        System.out.printf("Training with %d %dx%d games...\n", trainingGames, boardSize, boardSize);
        System.out.println();
	    GameSimulator trainingSimulator = new GameSimulator(trainingGames, boardSize);
        GameSimulatorResults trainingResults = trainingSimulator.learn(initialWeights, learningRate, graduallyDecreaseLearningRate, randomize);
        System.out.println(trainingResults.toString());

        // test trained player against random player
        System.out.println();
        System.out.printf("Testing trained player (X) against a random player (O) with %d %dx%d games...\n", testGames, boardSize, boardSize);
        System.out.printf("Learned weights: %s\n", Arrays.toString(trainingResults.getFinalWeights()));
        LearningPlayer trainedPlayerX = new LearningPlayer(Mark.X, trainingResults.getFinalWeights(), randomize);
        Player[] testPlayers = {trainedPlayerX, new RandomPlayer(Mark.O)};
        GameSimulator testSimulator = new GameSimulator(testGames, boardSize);
        GameSimulatorResults testResults = testSimulator.simulate(testPlayers);
        System.out.println(testResults.toString());
    }
}
