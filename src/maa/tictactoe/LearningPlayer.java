package maa.tictactoe;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * A player that chooses the next move based on an linear approximation of the target function calculated using
 * the current weights and the current board features.
 *
 * Created by irodrigo on 8/20/15.
 */
public class LearningPlayer extends Player {

    private double weights[];
    private boolean randomize;
    private Random random;

    /**
     * Create a new learning player with the given mark and initial weights.
     * @param mark The player's mark.
     * @param weights The initial weights.
     * @param randomize If true the player randomly picks one of the best moves, in case there's more than one.
     */
    public LearningPlayer(Mark mark, double weights[], boolean randomize){
        super(mark);
        assert(weights.length == Board.FEATURE_COUNT);

        this.weights = weights;
        this.randomize = randomize;
        if (randomize) {
            random = new Random();
        }
    }

    @Override
    public Coordinate nextMove(Board board) {
        return bestMove(board, board.emptyCoordinates());
    }

    /**
     * Pick the best move from the given legal moves and the given board.
     * The best move is the one that maximizes the value of the resulting board, according to the linear approximation
     * of the target function given by the current weights.
     * @param board The board.
     * @param moves The legal moves.
     * @return The best move or null if no moves are provided.
     */
    public Coordinate bestMove(Board board, Set<Coordinate> moves){
        if (moves.isEmpty()){
            return null;
        }
        List<Coordinate> bestMoves = new ArrayList<>();
        double bestValue = 0;
        for (Coordinate m : moves){
            double value = valueAfterMove(board, m);
            if (bestMoves.isEmpty() || value > bestValue) {
                bestMoves.clear();
                bestMoves.add(m);
                bestValue = value;
            }
            else if (value == bestValue) {
                bestMoves.add(m);
            }
        }
        int bestMoveIndex = randomize ? random.nextInt(bestMoves.size()) : 0;
        return bestMoves.get(bestMoveIndex);
    }

    /**
     * Calculate the value of the given board after performing the given move.
     * @param board The board.
     * @param move The move.
     * @return The value of the given board after performing the given move.
     */
    public double valueAfterMove(Board board, Coordinate move){
        Board boardCopy = new Board(board);
        boardCopy.setMark(move.row, move.column, getMark());
        return value(boardCopy);
    }

    /**
     * Calculate the value of the board according to the function Vop: Board -> R /
     * V(b) = w[0] + w[1]*x1 + w[2]*x2 + w[3]*x3 + w[4]*x4 + w[5]*x5 + w[6]*x6
     * where w[0]..w[6] are the current weights and x1..x6 the board features.
     * @param board The board.
     * @return The value of the board.
     */
    public double value(Board board){
        double value = 0;
        int[] features = board.features(getMark());
        for(int i = 0; i < weights.length; i++){
            value += features[i] * weights[i];
        }
        return value;
    }

    public double[] getWeights() {
        return weights;
    }

    public void setWeights(double[] weights) {
        this.weights = weights;
    }
}
