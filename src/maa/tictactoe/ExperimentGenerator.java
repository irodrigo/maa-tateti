package maa.tictactoe;

/**
 * The Experiment Generator takes as input the current hypothesis (currently learned function) and outputs a new problem
 * (i.e., initial board state) for the Performance System to explore.
 * Its role is to pick new practice problems that will maximize the learning rate of the overall system.
 * In this case, the Experiment Generator follows a very simple strategy: it always proposes the same initial game board
 * to begin a new game. More sophisticated strategies could involve creating board positions designed to explore
 * particular regions of the state space.
 *
 * Created by irodrigo on 8/23/15.
 */
public class ExperimentGenerator {

    private int size;

    public ExperimentGenerator(int size) {
        this.size = size;
    }

    public int getSize(){
        return size;
    }

    /**
     * Generate an empty board of the given size.
     * @return The created board.
     */
    public Board generateExperiment() {
        return new Board(size);
    }

}
