package maa.tictactoe;

/**
 * The marks used in the game, X and O.
 * Created by irodrigo on 8/19/15.
 */
public enum Mark {

    X, O;

    /**
     * Return the opposite of the given mark.
     * @return The opposite of the given mark.
     */
    public Mark opposite(){
        return this == X ? Mark.O : Mark.X;
    }
}
