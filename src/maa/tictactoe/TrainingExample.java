package maa.tictactoe;

/**
 * A basic data type to represent a training example.
 * A training example consists of a board and it's training value.
 *
 * Created by irodrigo on 8/23/15.
 */
public class TrainingExample {

    private Board board;
    private double trainingValue;

    public TrainingExample(Board board, double trainingValue) {
        this.board = board;
        this.trainingValue = trainingValue;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public double getTrainingValue() {
        return trainingValue;
    }

    public void setTrainingValue(double trainingValue) {
        this.trainingValue = trainingValue;
    }
}
