package maa.tictactoe;

import java.util.List;

/**
 * The Generalizer takes as input the training examples and produces an output hypothesis that is its estimate of the
 * target function. It generalizes from the specific training examples, hypothesizing a general function that covers
 * these examples and other cases beyond the training examples.
 * In this case, the Generalizer corresponds to the LMS algorithm, and the output hypothesis is the value function
 * described by the learned weights.
 *
 * Created by irodrigo on 8/23/15.
 */
public class Generalizer {

    /**
     * Return updated weights from the current weights and the given training examples.
     * @param trainingExamples The training examples.
     * @param mark The mark of the current player.
     * @param currentWeights The current weights.
     * @param learningRate The learning rate in the [0, 1) range.
     * @param randomize Flag to indicate if the player adds a random factor to the selection of the best board.
     * @return The updated weights.
     * @throws IllegalArgumentException If no training examples are provided, or if the weights count is not
     * Board.FEATURE_COUNT, or if the learning rate is out of the [0, 1) range, or if no mark provided.
     */
    public double[] updatedWeights (List<TrainingExample> trainingExamples, Mark mark, double[] currentWeights, double learningRate, boolean randomize) throws IllegalArgumentException{

        if (trainingExamples == null || trainingExamples.isEmpty()) {
            throw new IllegalArgumentException("at least one training example needed");
        }
        if (currentWeights.length != Board.FEATURE_COUNT) {
            throw new IllegalArgumentException("exactly Board.FEATURE_COUNT weights needed");
        }
        if (learningRate < 0 || learningRate > 1) {
            throw new IllegalArgumentException("learning rate must be between 0 and 1");
        }
        if (mark == null) {
            throw new IllegalArgumentException("mark can't be null");
        }

        double[] updatedWeights = currentWeights.clone();
        LearningPlayer player = new LearningPlayer(mark, updatedWeights, randomize);

        for (TrainingExample example : trainingExamples) {
            Board board = example.getBoard();
            double value = player.value(board);
            int[] features = board.features(mark);
            for (int w = 0; w < updatedWeights.length; w++) {
                double trainingValue = example.getTrainingValue();
                updatedWeights[w] += learningRate * (trainingValue - value) * features[w];
            }
            player.setWeights(updatedWeights);
        }

        return updatedWeights;
    }

}
