package maa.tictactoe;

import java.util.Arrays;

/**
 * Basic data type to store game simulation results.
 *
 * Created by irodrigo on 8/23/15.
 */
public class GameSimulatorResults {

    private int wins[];
    private int draws;
    private double[] finalWeights;

    public GameSimulatorResults () {
        this.draws = 0;
        this.wins = new int[Mark.values().length];
    }

    public int wins(Mark mark) {
        return wins[mark.ordinal()];
    }

    public int draws() {
        return draws;
    }

    public int games() {
        int games = draws;
        for (Mark m : Mark.values()) {
            games += wins(m);
        }
        return games;
    }

    public void update(Mark winner) {
        if (winner == null) {
            draws++;
        }
        else {
            wins[winner.ordinal()]++;
        }
    }

    public double[] getFinalWeights() {
        return finalWeights;
    }

    public void setFinalWeights(double[] finalWeights) {
        this.finalWeights = finalWeights;
    }

    @Override
    public String toString() {

        String str = "Game results:\n";
        int games = games();
        str += String.format("Played %d games\n", games);

        for(Mark mark : Mark.values()){
            int wins = wins(mark);
            str += String.format("%d (%d%%) wins by %s\n", wins, 100 * wins / games, mark.toString());
        }

        int draws = draws();
        str += String.format("%d (%d%%) draws\n", draws, 100 * draws / games);

        if (getFinalWeights() != null) {
            str += String.format("Final weights after learning: %s\n", Arrays.toString(getFinalWeights()));
        }

        return str;
    }
}
