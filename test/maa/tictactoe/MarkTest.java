package maa.tictactoe;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by irodrigo on 8/24/15.
 */
public class MarkTest {

    @Test
    public void testOpposite() throws Exception {
        assertEquals(Mark.X, Mark.O.opposite());
        assertEquals(Mark.O, Mark.X.opposite());
    }
}