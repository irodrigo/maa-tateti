package maa.tictactoe;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by irodrigo on 8/24/15.
 */
public class LearningPlayerTest {

    /**
     * Generate random boards in valid states for the player to play.
     * All generated boards are ready for the player to play his turn.
     * @param size The size of the boards.
     * @param mark The player's mark.
     * @return
     */
    public List<Board> generateBoards(int size, Mark mark) {
        PerformanceSystem performanceSystem = new PerformanceSystem();
        double[] weights = {1, 2, 3, 4, 5, 6, 7};
        Board board = new Board(size);
        List<Board> allBoards = performanceSystem.play(board, weights, mark, true);
        List<Board> playerBoards = new ArrayList<>();
        for (int i = 0; i < allBoards.size() - 1; i += 2) {
            playerBoards.add(allBoards.get(i));
        }
        return playerBoards;
    }

    @Test
    public void testNextMove() throws Exception {
        double[] weights = {1, 2, 3, 4, 5, 6, 7};
        LearningPlayer player = new LearningPlayer(Mark.X, weights, false);
        for (Board board : generateBoards(4, player.getMark())) {
            Coordinate nextMove = player.nextMove(board);
            assertEquals(player.bestMove(board, board.emptyCoordinates()), nextMove);
        }
    }

    @Test
    public void testBestMove() throws Exception {
        double[] weights = {1, 2, 3, 4, 5, 6, 7};
        LearningPlayer player = new LearningPlayer(Mark.X, weights, false);
        for (Board board : generateBoards(4, player.getMark())) {
            Coordinate bestMove = player.bestMove(board, board.emptyCoordinates());
            Board bestBoard = new Board(board);
            bestBoard.setMark(bestMove.row, bestMove.column, player.getMark());
            for (Coordinate testMove : board.emptyCoordinates()) {
                Board testBoard = new Board(board);
                testBoard.setMark(testMove.row, testMove.column, player.getMark());
                assertTrue(player.value(testBoard) <= player.value(bestBoard));
            }
        }
    }

    @Test
    public void testValueAfterMove() throws Exception {
        double[] weights = {1, 2, 3, 4, 5, 6, 7};
        LearningPlayer player = new LearningPlayer(Mark.X, weights, false);
        for (Board board : generateBoards(4, player.getMark())) {
            Coordinate move = player.nextMove(board);
            Board nextBoard = new Board(board);
            nextBoard.setMark(move.row, move.column, player.getMark());
            double expectedValue = player.value(nextBoard);
            double value = player.valueAfterMove(board, move);
            assertEquals(expectedValue, value, 0);
        }
    }

    @Test
    public void testValue() throws Exception {
        double[] weights = {1, 2, 3, 4, 5, 6, 7};
        LearningPlayer player = new LearningPlayer(Mark.X, weights, false);
        for (Board board : generateBoards(4, player.getMark())) {
            double expectedValue = 0;
            int[] features = board.features(player.getMark());
            for(int i = 0; i < weights.length; i++){
                expectedValue += features[i] * weights[i];
            }

            double value = player.value(board);

            assertEquals(expectedValue, value, 0);
        }
    }

    @Test
    public void testGetWeights() throws Exception {
        double[] weights = {1, 2, 3, 4, 5, 6, 7};
        LearningPlayer player = new LearningPlayer(Mark.X, weights, false);
        assertArrayEquals(weights, player.getWeights(), 0);
    }

    @Test
    public void testSetWeights() throws Exception {
        double[] weights = {1, 2, 3, 4, 5, 6, 7};
        LearningPlayer player = new LearningPlayer(Mark.X, weights, false);
        assertArrayEquals(weights, player.getWeights(), 0);

        double[] weights2 = {8, 9, 10, 11, 12, 13, 14};
        player.setWeights(weights2);
        assertArrayEquals(weights2, player.getWeights(), 0);
    }

    @Test
    public void testPlay() throws Exception {
        double[] weights = {1, 2, 3, 4, 5, 6, 7};
        LearningPlayer player = new LearningPlayer(Mark.X, weights, false);
        for (Board board : generateBoards(4, player.getMark())) {
            Board expectedBoard = new Board(board);
            Coordinate nextMove = player.nextMove(board);
            expectedBoard.setMark(nextMove.row, nextMove.column, player.getMark());

            Board playedBoard = player.play(board);
            assertEquals(expectedBoard, playedBoard);
        }

    }
}