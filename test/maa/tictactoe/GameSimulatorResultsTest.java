package maa.tictactoe;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by irodrigo on 8/24/15.
 */
public class GameSimulatorResultsTest {

    @Test
    public void testWins() throws Exception {
        GameSimulatorResults results = new GameSimulatorResults();
        assertEquals(0, results.wins(Mark.X));
        assertEquals(0, results.wins(Mark.O));

        results.update(Mark.X);
        assertEquals(1, results.wins(Mark.X));
        assertEquals(0, results.wins(Mark.O));

        results.update(Mark.X);
        assertEquals(2, results.wins(Mark.X));
        assertEquals(0, results.wins(Mark.O));
    }

    @Test
    public void testDraws() throws Exception {
        GameSimulatorResults results = new GameSimulatorResults();
        assertEquals(0, results.draws());

        results.update(Mark.X);
        assertEquals(0, results.draws());

        results.update(Mark.O);
        assertEquals(0, results.draws());

        results.update(null);
        assertEquals(1, results.draws());

        results.update(null);
        assertEquals(2, results.draws());
    }

    @Test
    public void testGetGames() throws Exception {
        GameSimulatorResults results = new GameSimulatorResults();
        assertEquals(0, results.games());

        results.update(Mark.X);
        assertEquals(1, results.games());

        results.update(Mark.O);
        assertEquals(2, results.games());

        results.update(null);
        assertEquals(3, results.games());
    }

    @Test
    public void testUpdate() throws Exception {
        GameSimulatorResults results = new GameSimulatorResults();
        assertEquals(0, results.games());
        assertEquals(0, results.wins(Mark.X));
        assertEquals(0, results.wins(Mark.O));
        assertEquals(0, results.draws());

        results.update(Mark.X);
        assertEquals(1, results.games());
        assertEquals(1, results.wins(Mark.X));
        assertEquals(0, results.wins(Mark.O));
        assertEquals(0, results.draws());

        results.update(Mark.O);
        assertEquals(2, results.games());
        assertEquals(1, results.wins(Mark.X));
        assertEquals(1, results.wins(Mark.O));
        assertEquals(0, results.draws());

        results.update(null);
        assertEquals(3, results.games());
        assertEquals(1, results.wins(Mark.X));
        assertEquals(1, results.wins(Mark.O));
        assertEquals(1, results.draws());
    }

    @Test
    public void testGetFinalWeights() throws Exception {
        GameSimulatorResults results = new GameSimulatorResults();
        assertNull(results.getFinalWeights());

        double[] finalWeights = {0, 1, 2, 3};
        results.setFinalWeights(finalWeights);
        assertArrayEquals(finalWeights, results.getFinalWeights(), 0);
    }

    @Test
    public void testSetFinalWeights() throws Exception {
        GameSimulatorResults results = new GameSimulatorResults();
        assertNull(results.getFinalWeights());

        double[] finalWeights = {0, 1, 2, 3};
        results.setFinalWeights(finalWeights);
        assertArrayEquals(finalWeights, results.getFinalWeights(), 0);
    }
}