package maa.tictactoe;

import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by irodrigo on 8/24/15.
 */
public class BoardTest {

    @org.junit.Test
    public void testCopyConstructor() throws Exception {
        int size = 4;
        Board board = new Board(size);
        board.setMark(3, 2, Mark.X);
        board.setMark(1, 2, Mark.O);
        board.setMark(0, 0, Mark.X);
        Board copy = new Board(board);
        assertEquals(size, copy.getSize());
        for (int r = 0; r < size; r++) {
            for (int c = 0; c < size; c++) {
                assertEquals(board.getMark(r, c), copy.getMark(r, c));
            }
        }

        try {
            board = new Board(null);
            fail("trying to copy a null board should throw an exception");
        }
        catch (IllegalArgumentException e) {
            // expected
        }

    }

    @org.junit.Test
    public void testGetSize() throws Exception {
        Board board = new Board(8);
        assertEquals(8, board.getSize());
    }

    @org.junit.Test
    public void testGetMark() throws Exception {
        Board board = new Board(7);
        board.setMark(5, 4, Mark.X);
        board.setMark(6, 6, Mark.O);
        assertEquals(Mark.X, board.getMark(5, 4));
        assertEquals(Mark.O, board.getMark(6, 6));
        assertEquals(null, board.getMark(0, 0));
    }

    @org.junit.Test
    public void testEmptyCoordinates() throws Exception {
        Board board = new Board(9);
        assertEquals(9 * 9, board.emptyCoordinates().size());

        board.setMark(7, 8, Mark.X);
        board.setMark(0, 0, Mark.O);
        assertEquals(9 * 9 - 2, board.emptyCoordinates().size());
    }

    @org.junit.Test
    public void testSetMark() throws Exception {
        Board board = new Board(10);
        board.setMark(9, 9, Mark.X);
        board.setMark(0, 0, Mark.O);
        assertEquals(Mark.X, board.getMark(9, 9));
        assertEquals(Mark.O, board.getMark(0, 0));
        assertEquals(null, board.getMark(1, 1));

        try {
            board.setMark(0, 0, Mark.X);
            fail("setMark should throw an exception");
        }
        catch (IllegalArgumentException e) {
            // expected
        }

        try {
            board.setMark(1, 1, null);
            fail("setMark should throw an exception");
        }
        catch (IllegalArgumentException e) {
            // expected
        }
    }

    @org.junit.Test
    public void testAllLines() throws Exception {
        int size = 15;
        Board board = new Board(size);
        Set<List<Coordinate>> allLines = board.allLines();
        assertEquals(2 * size + 2, allLines.size());
        for (List<Coordinate> line : allLines) {
            assertEquals(size, line.size());
        }
    }

    @org.junit.Test
    public void testCalculateLinesWithWinningChance() throws Exception {
        int size = 4;
        Board board = new Board(size);
        board.setMark(0, 0, Mark.X);
        board.setMark(3, 3, Mark.X);
        board.setMark(1, 2, Mark.O);
        board.setMark(2, 1, Mark.O);
        board.setMark(1, 1, Mark.O);
        /**
         X _ _ _
         _ O O _
         _ O _ _
         _ _ _ X
         */

        Set<List<Coordinate>> allLinesWithWinningChanceForX = board.calculateLinesWithWinningChance(Mark.X, 0);
        assertEquals(4, allLinesWithWinningChanceForX.size());
        for (List<Coordinate> line : allLinesWithWinningChanceForX) {
            assertEquals(size, line.size());
        }

        Set<List<Coordinate>> allLinesWithWinningChanceForO = board.calculateLinesWithWinningChance(Mark.O, 0);
        assertEquals(5, allLinesWithWinningChanceForO.size());
        for (List<Coordinate> line : allLinesWithWinningChanceForO) {
            assertEquals(size, line.size());
        }
    }

    @org.junit.Test
    public void testWinner() throws Exception {
        int size = 4;
        Board board = new Board(size);
        board.setMark(0, 0, Mark.X);
        board.setMark(0, 1, Mark.X);
        board.setMark(0, 2, Mark.X);
        board.setMark(0, 3, Mark.O);
        board.setMark(1, 0, Mark.O);
        board.setMark(1, 1, Mark.O);
        board.setMark(1, 2, Mark.O);
        board.setMark(1, 3, Mark.X);
        board.setMark(2, 0, Mark.X);
        board.setMark(2, 1, Mark.X);
        board.setMark(2, 2, Mark.X);
        board.setMark(2, 3, Mark.O);
        board.setMark(3, 0, Mark.O);
        board.setMark(3, 1, Mark.O);
        board.setMark(3, 2, Mark.O);
        board.setMark(3, 3, Mark.X);
        /**
         X X X O
         O O O X
         X X X O
         O O O X
         */

        // should be a draw
        assertTrue(board.isFinished());
        assertNull(board.winner());

        board = new Board(size);
        board.setMark(0, 0, Mark.X);
        board.setMark(0, 1, Mark.X);
        board.setMark(0, 2, Mark.X);
        board.setMark(0, 3, Mark.O);
        board.setMark(1, 0, Mark.O);
        board.setMark(1, 1, Mark.X);
        board.setMark(1, 2, Mark.O);
        board.setMark(1, 3, Mark.O);
        board.setMark(2, 0, Mark.O);
        board.setMark(2, 1, Mark.O);
        board.setMark(2, 2, Mark.X);
        board.setMark(2, 3, Mark.O);
        board.setMark(3, 1, Mark.O);
        board.setMark(3, 2, Mark.X);
        board.setMark(3, 3, Mark.X);
        /**
         X X X O
         O X O O
         O O X O
         _ O X X
         */

        // X should be the winner
        assertTrue(board.isFinished());
        assertEquals(Mark.X, board.winner());

        board = new Board(size);
        board.setMark(0, 0, Mark.X);
        board.setMark(0, 1, Mark.X);
        board.setMark(0, 2, Mark.X);
        board.setMark(0, 3, Mark.O);
        board.setMark(1, 0, Mark.O);
        board.setMark(1, 1, Mark.X);
        board.setMark(1, 2, Mark.O);
        board.setMark(1, 3, Mark.O);
        board.setMark(2, 0, Mark.O);
        board.setMark(2, 1, Mark.O);
        board.setMark(2, 2, Mark.X);
        board.setMark(3, 0, Mark.O);
        board.setMark(3, 1, Mark.X);
        board.setMark(3, 2, Mark.X);
        /**
         X X X O
         O X O O
         O O X _
         O X X _
         */

        // O should be the winner
        assertTrue(board.isFinished());
        assertEquals(Mark.O, board.winner());

    }

    @org.junit.Test
    public void testIsFinished() throws Exception {
        int size = 3;
        Board board = new Board(size);

        assertFalse(board.isFinished());

        board.setMark(0, 0, Mark.X);
        board.setMark(0, 1, Mark.X);
        board.setMark(0, 2, Mark.O);
        board.setMark(1, 0, Mark.O);
        board.setMark(1, 1, Mark.O);
        board.setMark(1, 2, Mark.X);
        board.setMark(2, 0, Mark.X);
        board.setMark(2, 1, Mark.X);
        /**
         X X O
         O O X
         X X _
         */

        assertFalse(board.isFinished());

        board = new Board(size);
        board.setMark(0, 0, Mark.X);
        board.setMark(0, 1, Mark.X);
        board.setMark(0, 2, Mark.X);
        board.setMark(1, 0, Mark.O);
        board.setMark(1, 1, Mark.O);
        /**
         X X X
         O O _
         _ _ _
         */

        assertTrue(board.isFinished());

    }

    @org.junit.Test
    public void testFeatures() throws Exception {
        int size = 4;
        Board board = new Board(size);
        board.setMark(0, 0, Mark.X);
        board.setMark(3, 3, Mark.X);
        board.setMark(1, 2, Mark.O);
        board.setMark(2, 1, Mark.O);
        board.setMark(1, 1, Mark.O);
        /**
         X _ _ _
         _ O O _
         _ O _ _
         _ _ _ X
         */

        int[] featuresX = board.features(Mark.X);
        int[] featuresO = board.features(Mark.O);

        assertArrayEquals(new int[] {1, 4, 5, 0, 0, 0, 0}, featuresX);
        assertArrayEquals(new int[] {1, 5, 4, 0, 0, 0, 0}, featuresO);


        board = new Board(size);
        board.setMark(0, 0, Mark.X);
        board.setMark(0, 1, Mark.X);
        board.setMark(0, 2, Mark.X);
        board.setMark(0, 3, Mark.O);
        board.setMark(1, 0, Mark.O);
        board.setMark(1, 1, Mark.X);
        board.setMark(1, 2, Mark.O);
        board.setMark(1, 3, Mark.O);
        board.setMark(2, 0, Mark.O);
        board.setMark(2, 1, Mark.O);
        board.setMark(2, 2, Mark.X);
        board.setMark(2, 3, Mark.O);
        board.setMark(3, 1, Mark.O);
        board.setMark(3, 2, Mark.X);
        board.setMark(3, 3, Mark.X);
        /**
         X X X O
         O X O O
         O O X O
         _ O X X
         */

        featuresX = board.features(Mark.X);
        featuresO = board.features(Mark.O);

        assertArrayEquals(new int[]{1, 1, 1, 0, 1, 1, 0}, featuresX);
        assertArrayEquals(new int[]{1, 1, 1, 1, 0, 0, 1}, featuresO);

        board = new Board(size);
        board.setMark(0, 0, Mark.X);
        board.setMark(0, 1, Mark.X);
        board.setMark(0, 2, Mark.X);
        board.setMark(0, 3, Mark.O);
        board.setMark(1, 0, Mark.O);
        board.setMark(1, 1, Mark.X);
        board.setMark(1, 2, Mark.O);
        board.setMark(1, 3, Mark.O);
        board.setMark(2, 0, Mark.O);
        board.setMark(2, 1, Mark.O);
        board.setMark(2, 2, Mark.X);
        board.setMark(3, 0, Mark.O);
        board.setMark(3, 1, Mark.X);
        board.setMark(3, 2, Mark.X);
        /**
         X X X O
         O X O O
         O O X _
         O X X _
         */

        featuresX = board.features(Mark.X);
        featuresO = board.features(Mark.O);

        assertArrayEquals(new int[]{1, 1, 2, 1, 0, 0, 1}, featuresX);
        assertArrayEquals(new int[]{1, 2, 1, 0, 1, 1, 0}, featuresO);
    }

    @Test
    public void testEquals() throws Exception {
        Board board1 = new Board(3);
        Board board2 = new Board(3);
        Board board3 = new Board(4);

        assertEquals(board1, board2);
        assertNotEquals(board1, board3);

        board1.setMark(0, 0, Mark.X);
        assertNotEquals(board1, board2);

        board2.setMark(0, 0, Mark.X);
        assertEquals(board1, board2);

        board1.setMark(0, 1, Mark.O);
        assertNotEquals(board1, board2);

        board2.setMark(0, 1, Mark.O);
        assertEquals(board1, board2);
    }

    @Test
    public void testHashCode() throws Exception {
        Board board1 = new Board(3);
        Board board2 = new Board(3);
        Board board3 = new Board(4);

        assertEquals(board1.hashCode(), board2.hashCode());
        assertNotEquals(board1.hashCode(), board3.hashCode());

        board1.setMark(0, 0, Mark.X);
        assertNotEquals(board1.hashCode(), board2.hashCode());

        board2.setMark(0, 0, Mark.X);
        assertEquals(board1.hashCode(), board2.hashCode());

        board1.setMark(0, 1, Mark.O);
        assertNotEquals(board1.hashCode(), board2.hashCode());

        board2.setMark(0, 1, Mark.O);
        assertEquals(board1.hashCode(), board2.hashCode());
    }
}