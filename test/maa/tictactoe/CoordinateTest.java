package maa.tictactoe;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by irodrigo on 8/24/15.
 */
public class CoordinateTest {

    @Test
    public void testEquals() throws Exception {
        assertEquals(new Coordinate(1, 2), new Coordinate(1, 2));
        Coordinate coordinate = new Coordinate(1, 2);
        assertEquals(coordinate, coordinate);

        assertNotEquals(new Coordinate(1, 2), new Coordinate(2, 1));
        assertNotEquals(new Coordinate(1, 2), null);
        assertNotEquals(null, new Coordinate(2, 1));
        assertNotEquals(new Coordinate(1, 2), new Object());
        assertNotEquals(new Object(), new Coordinate(1, 2));
    }

    @Test
    public void testHashCode() throws Exception {
        assertEquals(new Coordinate(1, 2).hashCode(), new Coordinate(1, 2).hashCode());
        assertNotEquals(new Coordinate(2, 1).hashCode(), new Coordinate(1, 2).hashCode());
    }
}