package maa.tictactoe;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by irodrigo on 8/24/15.
 */
public class CriticTest {

    @Test
    public void testTrainingExamples() throws Exception {

        int size = 3;

        // generate history

        List<Board> history = new ArrayList<>();

        Board board = new Board(size);
        history.add(board);

        board.setMark(0, 0, Mark.X);
        history.add(board);

        board = new Board(board);
        board.setMark(1, 0, Mark.O);
        history.add(board);

        board = new Board(board);
        board.setMark(0, 1, Mark.X);
        history.add(board);

        board = new Board(board);
        board.setMark(1, 1, Mark.O);
        history.add(board);

        board = new Board(board);
        board.setMark(0, 2, Mark.X);
        history.add(board);

        // test for X

        Critic critic = new Critic();
        double[] weights = {0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5};
        Mark mark = Mark.X;
        LearningPlayer player = new LearningPlayer(mark, weights, false);
        List<TrainingExample> examples = critic.trainingExamples(history, mark, weights, false);

        assertEquals(3, examples.size());

        assertEquals(history.get(0), examples.get(0).getBoard());
        assertEquals(player.value(history.get(2)), examples.get(0).getTrainingValue(), 0);

        assertEquals(history.get(2), examples.get(1).getBoard());
        assertEquals(player.value(history.get(4)), examples.get(1).getTrainingValue(), 0);

        assertEquals(history.get(5), examples.get(2).getBoard());
        assertEquals(100, examples.get(2).getTrainingValue(), 0);

        // repeat for O

        mark = Mark.O;
        player = new LearningPlayer(mark, weights, false);
        examples = critic.trainingExamples(history, mark, weights, false);

        assertEquals(3, examples.size());

        assertEquals(history.get(1), examples.get(0).getBoard());
        assertEquals(player.value(history.get(3)), examples.get(0).getTrainingValue(), 0);

        assertEquals(history.get(3), examples.get(1).getBoard());
        assertEquals(player.value(history.get(5)), examples.get(1).getTrainingValue(), 0);

        assertEquals(history.get(5), examples.get(2).getBoard());
        assertEquals(-100, examples.get(2).getTrainingValue(), 0);

    }
}