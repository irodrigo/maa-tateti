package maa.tictactoe;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by irodrigo on 8/24/15.
 */
public class ExperimentGeneratorTest {

    @Test
    public void testGetSize() throws Exception {
        ExperimentGenerator e = new ExperimentGenerator(7);
        assertEquals(7, e.getSize());
    }

    @Test
    public void testGenerateExperiment() throws Exception {
        ExperimentGenerator e = new ExperimentGenerator(5);
        assertEquals(5, e.generateExperiment().getSize());
    }
}