package maa.tictactoe;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by irodrigo on 8/24/15.
 */
public class PerformanceSystemTest {

    @Test
    public void testPlay() throws Exception {
        PerformanceSystem performanceSystem = new PerformanceSystem();
        double[] weights = {1, 2, 3, 4, 5, 6, 7};
        Mark startingPlayer = Mark.X;

        // calcuate expected history
        Board board = new Board(5);
        LearningPlayer player1 = new LearningPlayer(startingPlayer, weights.clone(), false);
        LearningPlayer player2 = new LearningPlayer(startingPlayer.opposite(), weights.clone(), false);
        List<Board> expectedHistory = new ArrayList<Board>();
        expectedHistory.add(board);
        Board currentBoard = board;
        while(!currentBoard.isFinished()){
            Board board1 = player1.play(currentBoard);
            expectedHistory.add(board1);
            currentBoard = board1;
            if (!currentBoard.isFinished()) {
                Board board2 = player2.play(currentBoard);
                expectedHistory.add(board2);
                currentBoard = board2;
            }
        }

        List<Board> history = performanceSystem.play(board, weights, startingPlayer, false);

        assertEquals(expectedHistory, history);
    }
}