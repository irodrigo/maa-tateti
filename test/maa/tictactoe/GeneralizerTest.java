package maa.tictactoe;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by irodrigo on 8/24/15.
 */
public class GeneralizerTest {

    @Test
    public void testUpdatedWeights() throws Exception {

        double[] currentWeights = {1, 1, 1, 1, 1, 1, 1};
        int size = 3;
        Mark mark = Mark.X;
        double learningRate = 0.1;

        List<TrainingExample> trainingExamples = new ArrayList<>();
        Board board = new Board(size);
        trainingExamples.add(new TrainingExample(board, 1));

        board = new Board(board);
        board.setMark(0, 0, Mark.X);
        board.setMark(1, 0, Mark.O);
        trainingExamples.add(new TrainingExample(board, 3));

        Generalizer generalizer = new Generalizer();
        double[] updatedWeights = generalizer.updatedWeights(trainingExamples, mark, currentWeights, learningRate, false);

        assertEquals(currentWeights.length, updatedWeights.length);
        assertNotEquals(currentWeights, updatedWeights);


        double[] expectedUpdatedWeights = currentWeights.clone();
        LearningPlayer player = new LearningPlayer(mark, expectedUpdatedWeights, false);

        for (TrainingExample example : trainingExamples) {
            Board exampleBoard = example.getBoard();
            double value = player.value(exampleBoard);
            int[] features = exampleBoard.features(mark);
            for (int w = 0; w < expectedUpdatedWeights.length; w++) {
                double trainingValue = example.getTrainingValue();
                expectedUpdatedWeights[w] += learningRate * (trainingValue - value) * features[w];
            }
            player.setWeights(expectedUpdatedWeights);
        }

        assertArrayEquals(expectedUpdatedWeights, updatedWeights, 0);
    }
}