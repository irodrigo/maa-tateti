package maa.tictactoe;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by irodrigo on 8/24/15.
 */
public class GameSimulatorTest {

    @Test
    public void testLearn() throws Exception {
        int size = 5;
        int games = 10;
        double[] weights = {0, 0, 0, 0, 0, 0, 0};
        double learningRate = 0.1;
        boolean decreaseLearningRate = true;

        GameSimulator simulator = new GameSimulator(games, size);
        GameSimulatorResults results = simulator.learn(weights, learningRate, decreaseLearningRate, false);
        assertNotEquals(weights, results.getFinalWeights());
        assertEquals(games, results.games());
    }

    @Test
    public void testSimulate() throws Exception {
        int size = 5;
        int games = 10;
        double[] weights = {0, 0, 0, 0, 0, 0, 0};
        LearningPlayer playerX = new LearningPlayer(Mark.X, weights.clone(), false);
        LearningPlayer playerO = new LearningPlayer(Mark.O, weights.clone(), false);
        LearningPlayer[] players = {playerX, playerO};

        GameSimulator simulator = new GameSimulator(games, size);
        GameSimulatorResults results = simulator.simulate(players);
        assertNull(results.getFinalWeights());
        assertEquals(games, results.games());
        assertArrayEquals(weights, playerX.getWeights(), 0);
        assertArrayEquals(weights, playerO.getWeights(), 0);
    }
}